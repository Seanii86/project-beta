from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "id",
    ]


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "phone_number",
        "address",
        "id",
    ]
   
    
class Sales(ModelEncoder):
    model = SalesRecord
    properties = [
        "sales_person",
        "customer",
        "price",
        "automobile",
        "id",
    ]
    encoders = {
        "sales_person": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),
        "automobile": AutomobileVOEncoder(),   
    }

# Customer
@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerListEncoder)
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerListEncoder, safe=False)
        except:
            return JsonResponse({"message": "Could not create customer"}, status=400)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(customer, encoder=CustomerListEncoder, safe=False)
        except:
            return JsonResponse({"message": "Customer does not exist"}, status=404)
    elif request.method == "DELETE":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)

            props = ["name", "address", "phone_number"]
            for prop in props:
                setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(customer, encoder=CustomerListEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)


# Sales Person
@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonListEncoder, safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(salesperson, encoder=SalesPersonListEncoder, safe=False)
        except:
            return JsonResponse(
                {"message": "Could not create sales person"}, status=400
            )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            salesperson = SalesPerson.objects.get(id=pk)
            return JsonResponse(salesperson, encoder=SalesPersonListEncoder, safe=False)
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales person does not exist"}, status=404)
    elif request.method == "DELETE":
        try:
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.get(id=pk)

            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
                salesperson.save()
                return JsonResponse(
                    salesperson, encoder=SalesPersonListEncoder, safe=False
                )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)


# sales record
@require_http_methods(["GET", "POST"])
def api_sales_records(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales}, encoder=Sales
        )
    else:
        try:
            content = json.loads(request.body)

   
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile


            sales_person = SalesPerson.objects.get(employee_number=content["sales_person"])
            content["sales_person"] = sales_person
            
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer

            sales = SalesRecord.objects.create(**content)
            return JsonResponse(sales, encoder=Sales, safe=False)
        except:
            return JsonResponse(
                {"message": "Could not create Sales record"},
                status=400,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_record(request, pk):
    if request.method == "GET":
        try:
            sales = SalesRecord.objects.get(id=pk)
            return JsonResponse(sales, encoder=Sales, safe=False)
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Sales record does not exist"}, status=404)
    elif request.method == "DELETE":
        try:
            count, _ = SalesRecord.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            sales = SalesRecord.objects.get(id=pk)

            props = ["price"]
            for prop in props:
                if prop in content:
                    setattr(sales, prop, content[prop])
            sales.save()
            return JsonResponse(sales, encoder=Sales, safe=False)
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)
        
