from django.urls import path
from .views import (
    list_technicians,
    delete_technician,
    list_appointments,
    app_details,

)


urlpatterns = [
    path("technicians/", list_technicians, name="list_techs"),
    path("technicians/<int:pk>/", delete_technician, name="tech_delete"),
    path("appointments/", list_appointments, name="list_apps"),
    path("appointments/<int:pk>/", app_details, name="app_details")
]