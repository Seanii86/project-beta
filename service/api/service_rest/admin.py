from django.contrib import admin
from .models import AutomobileVO, Appointment

admin.site.register(AutomobileVO)
admin.site.register(Appointment)
