from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Appointment, Technician, AutomobileVO
from .encoders import AutoVOEncoder, TechnicianEncoder, AppointmentEncoder, AppointmentDetailEncoder


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"technicians": techs},
            encoder = TechnicianEncoder
        )
    else:
        content = json.loads(request.body)
        newTech = Technician.objects.create(**content)
        return JsonResponse(
            newTech,
            encoder = TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def delete_technician(request, pk):
    if request.method == "DELETE":
        count, _=Technician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder = AppointmentDetailEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(emp_number=content['technician'])
            content['technician'] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "invalid Employee number"},
                status = 400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def app_details(request, pk):
    if request.method == "GET":
        app = Appointment.objects.filter(app_vin = pk)
        return JsonResponse(
            app,
            encoder=AppointmentEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        app = Appointment.objects.get(id=pk)
        return JsonResponse(
            {"app": app},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        count, _=Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
