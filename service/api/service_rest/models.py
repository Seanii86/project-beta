from django.db import models
from django.urls import reverse

class Technician (models.Model):
    name = models.CharField(max_length=100)
    emp_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    auto_href = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    customer_name = models.CharField(max_length=100)
    date_time = models.DateTimeField(null=True)
    reason = models.TextField()
    app_vin = models.CharField(max_length=17)
    finished = models.BooleanField(default=False)
    vip = models.BooleanField(default=False, blank=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.customer_name}: {self.app_vin}'



