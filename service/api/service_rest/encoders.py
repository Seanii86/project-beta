from .models import Appointment, Technician, AutomobileVO
from common.json import ModelEncoder


class AutoVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "auto_href"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "emp_number", "id"]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date_time",
        "reason",
        "app_vin",
        "finished",
        "vip",
        "id"
    ]
    def get_extra_data(self, o):
        return {
            "technician": o.technician.name,
        }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "customer_name",
        "date_time",
        "reason",
        "app_vin",
        "finished",
        "vip",
        "id"
    ]

    def get_extra_data(self, o):
        try:
            AutomobileVO.objects.get(vin=o.app_vin)
            return {"vip": "Yes",   
            "technician": o.technician.name,
            "date_time": o.date_time.strftime("%m/%d/%Y at %I:%M %p")}
        except:
            return {"vip": "No",   
            "technician": o.technician.name,
            "date_time": o.date_time.strftime("%m/%d/%Y at %I:%M %p")}
