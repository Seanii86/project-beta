
<div align="center">

# CarCar
**CarCar is the premier solution for automobile dealership management.**

CarCar will make your responsibility for managing inventory, sales records, service records, employees, and customers **simpler.**


[Our Team](#our-team) | [Installation](#installation) | [Features](#features) | [Design](#design) | [Feedback](#feedback)

![](images/Logo.png)

</div>

# Features

### Service microservice
#### Within our service microservice you may:
* Add a technician to your emplyee database:
* Create a service appointment for a vehicle providing a VIN#, vehicle owners name, date & time of appointment, assign a technician, and the reason for service.
* View a list of ongoing appointments and mark each as "finished" or "canceled".
* View service history, which includes searching appointments by VIN# to keep track of service records.

#### The service microservice includes aspects such as:
* Django models written in python for the Technicians, Appointments, and the AutomobileVO designed with a poller to link your appointments to automobiles in your inventory. Just in case a customer has purchased from you, they will receive V.I.P treatment for their service!
* Views and registered urlpatterns to cooperate with GET, POST, PUT, and DELETE endpoints.

#### A table of endpoints for the Sales microservice to use for demonstration here:

| Action | Method | URL |
| --- | :---: | --- |
| List appointments | GET | `http://localhost:8080/services/appointments/`
| Create appointment | POST | `http://localhost:8080/services/appointments/`
| Get a specific appointment by VIN | GET | `http://localhost:8080/services/appointments/<str:vin>/`
| Get a specific appointment | GET | `http://localhost:8080/services/appointments/<int:pk>/`
| Update a specific appointment | PUT | `http://localhost:8080/services/appointments/<int:pk>/`
| Delete a specific appointment | DELETE | `http://localhost:8080/services/appointments/<int:pk>/`
| List technicians | GET | `http://localhost:8080/services/technicians/`
| Create technician | POST | `http://localhost:8080/services/technicians/`
| Get a specific technician  | GET | `http://localhost:8080/services/technicians/<int:pk>/`
| Delete a specific technician  | DELETE | `http://localhost:8080/services/technicians/<int:pk>/`

### Sales Microservice
#### Within our sales microservice you may:
* The sales app will record and monitor various data from transactions such as sales history, sales person, customers, and automobiles. 

* Created models of microservice to store data with AutomobileVO, sales person, sales record, and customer. Created forms for each sales record, sales person, customers. 
    * sales person form will need a name and employee number
    * customer form will need a name, phone number, and address
    * foreign keys in sales records contain their own dropdown menu so you can select from their respective table data
        * sales record form will use a foreign key to receive data on:
        * automobile: VIN of each automobile
        * sales_person: to place sales person to sold vehicle
        * customer: to receive name of customer who bought the vehicle 
        * price however is not a foreign key because it does not contain its own table

* Created sales person's history and their list of sales

* Created a poller to get automobile service data into Inventory

* Added the nav bar for front end ease of use to use forms and list. It includes history for sales person, potential customers, sales list, sales records, and sale's person's history. 
    * front end navigation to navigate forms and lists

* Created highlighted dropdown selection menu for sales person' history and which contain the following:
    * customers
    * VIN
    * sales price

#### A table of endpoints for the sales microservice to use for demonstration here:

| Action | Method | URL |
| --- | :---: | --- |
| List sales records | GET | `http://localhost:8080/api/sales_records/`
| Create a sales record | POST | `http://localhost:8080/api/sales_records/`
| Get a specific sales record by id | GET | `http://localhost:8080/api/sales_records/<int:id>/`
| Delete a sales record | DELETE | `http://localhost:8080/api/sales_records/<int:id>`
| Add a customer | POST | `http://localhost:8080/api/customers/`
| View customer detail | GET | `http://localhost:8080/api/customers/<int:id>/`
| Delete a specific customer | DELETE | `http://localhost:8080/api/customers/<int:id>/`
| Add a sales person | POST | `http://localhost:8080/api/sales_persons/`
| View sales persons details | POST | `http://localhost:8080/api/sales_persons/<int:id>/`
| Delete a specific sales person  | DELETE | `http://localhost:8080/api/sales_persons/<int:id>/`

### Inventory features

#### Within the inventory you have acces to:

* Auto form that allows the user to add their automobile specifications when adding to Inventory
    includes the following:
    * color
    * year
    * VIN
    * model 

* Auto list with the with the entered parameters that includes:
    * color
    * year
    * VIN
    * manufacturer
    * model

Manufacturer list that include the names of the automobile manufacturer.

Manufacturer form that lets the user enter the name of the manufacturer of their automobile

Vehicle model form that creates a form that users can specify:
    * picture of vehicle
    * manufacturer of vehicle
    * name of vehicle

Vehicle list that displays list of vehicles and their information entered by the user

#### A table of endpoints to use in insomnia for demonstration here:

| Action | Method | URL |
| --- | :---: | --- |
| List manufacturers | GET | `http://localhost:8100/api/manufacturers/`
| Create a manufactureer | POST | `http://localhost:8100/api/manufacturers/`
| Get a specific manufacturer details | GET | `http://localhost:8100/api/manufacturers/<int:id>/`
| Delete a manufacturer  | DELETE | `http://localhost:8100/api/manufacturers/<int:id>/`
| Add a vehicle model | POST | `http://localhost:8100/api/models/`
| View a list of vehicle models | GET | `http://localhost:8100/api/models//`
| Delete a specific vehicle model | DELETE | `http://localhost:8100/api/models/<int:id/>`
| Add a new entire automobile | POST | `http://localhost:8100/api/automobiles/`
| View an automobiles details by VIN | GET | `http://localhost:8100/api/automobiles/:vin/`
| Delete a specific automobile | DELETE | `	http://localhost:8100/api/automobiles/:vin/`

# Our Team
* Service Development - Sean Sainz
* Sales Development - Gary Lu


# Installation
1. Open your terminal and create a new directory to clone.
   * ``` mkdir <<Your directory name>> ```
2. cd into your new directory.
   * ``` cd <<Your directory name>> ```
3. git clone with HTTPS into your new directory.
    * ``` git clone https://gitlab.com/Seanii86/project-beta.git ```
4. cd into the project root
    * ``` cd project-beta ```
5. Open your Visual Studio Code editor (or editor of your choice).
    * ``` code . ```
6. Create a PostgreSQL database, build the images, and run the containers inside VSCode terminal.
    * ``` docker volume create beta-data ```
    * ``` docker-compose build ```
    * ``` docker-compose up ```
    #### Usefule commands:
    To delete a database: ``` docker volume rm beta-data ```
    
    Prune your containers/images: ``` docker containers prune -f ```

# Design

<div align="center">

![](images/GaryDiagram.png)

![](images/Sean%20Diagram.png)

</div>


## Feedback

#### Question, suggestions, any feedback is appreciated

#### Contanct us here:
* Sean - [GitLab](https://gitlab.com/Seanii86) - [LinkedIn](https://www.linkedin.com/in/seansainz/)

* Gary - [GitLab](https://gitlab.com/garyglu) - [LinkedIn](https://www.linkedin.com/in/gary-lu-2312191a1/)