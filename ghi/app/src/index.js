import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadComponents() {
  const response = await fetch('http://localhost:8100/api/manufacturers/')
  const response3 = await fetch('http://localhost:8100/api/models/')
  const response4 = await fetch('http://localhost:8100/api/automobiles/')

  if (response.ok) {
    const data = await response.json()
    const data3 = await response3.json()
    const data4 = await response4.json()
    root.render(
      <React.StrictMode>
        <App manufacturers={data.manufacturers}
          models={data3.models}
          autos={data4.autos} />
      </React.StrictMode>
    )
  } else {
    console.log('Something went wrong INDEX::::', response)
  }
}
loadComponents()
