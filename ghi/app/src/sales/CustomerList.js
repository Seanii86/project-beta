import React from "react"

class CustomerList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name:'',
            phone_number:'',
            address:'',
        }
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/customers/')
        if (response.ok) {
            const data = await response.json()
            this.setState({
                customers: data.customers
            })
        }
    }

    render () {
        return (
            <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>name</th>
                        <th>phone number</th>
                        <th>address</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.customers?.map(data => {
                        return (
                            <tr key={data.id}>
                                <td>{ data.name }</td>
                                <td>{ data.phone_number }</td>
                                <td>{ data.address }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            </>
        )
    }
}

export default CustomerList