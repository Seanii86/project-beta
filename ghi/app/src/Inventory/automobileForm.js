import React from "react"

class AutomobileForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            model_id: '',
            vin: '',
            year: '',
            color: '',
            models: [],

        }
    }


    handleModel = (event) => {
        const value = event.target.value
        this.setState({ model_id: value })
    }
    handleVin = (event) => {
        const value = event.target.value
        this.setState({ vin: value })
    }
    handleYear = (event) => {
        const value = event.target.value
        this.setState({ year: value })
    }
    handleColor = (event) => {
        const value = event.target.value
        this.setState({ color: value })
    }


    handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...this.state }

        delete data.models


        const url = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newInventoryItem = await response.json()
            const cleared = {
                model_id: '',
                vin: '',
                year: '',
                color: '',

            }
            this.setState(cleared)
        }
    }


    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            this.setState({ models: data.models })
        }
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add model to inventory</h1>
                        <form onSubmit={this.handleSubmit} id="create-a-vehicle-form">

                            <div className="form-floating mb-3">
                                <input placeholder="color" onChange={this.handleColor} value={this.state.color} required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Vehicle Color</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input placeholder="year" onChange={this.handleYear} value={this.state.year} required type="number" name="year" id="year" className="form-control" />
                                <label htmlFor="year">Manufacturing year</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input placeholder="vin" onChange={this.handleVin} value={this.state.vin} required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN#</label>
                            </div>

                            <div className="mb-3">
                                <select onChange={this.handleModel} value={this.state.model_id} required name="model" id="model" className="form-select">
                                    <option value="">Model</option>
                                    {this.state.models?.map(data => {
                                        return (
                                            <option key={data.id} value={data.id}>
                                                {data.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default AutomobileForm