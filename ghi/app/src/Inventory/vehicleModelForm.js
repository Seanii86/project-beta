import React from 'react';

class VehicleForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            pictureUrl: '',
            manufacturerId: '',

        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handlePicture = this.handlePicture.bind(this)
        this.handleModel = this.handleModel.bind(this)
        this.handleManufacturer = this.handleManufacturer.bind(this)
    }

    handleModel(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handlePicture(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value })
    }
    handleManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturerId: value })
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        data.manufacturer_id = data.manufacturerId
        data.picture_url = data.pictureUrl
        delete data.pictureUrl
        delete data.manufacturerId
        delete data.manufacturers

        const url = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json()
            const cleared = {
                name: '',
                pictureUrl: '',
                manufacturerId: '',
            }
            this.setState(cleared)
        }
    }


    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            this.setState({ manufacturers: data.manufacturers })
        }
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add model to inventory</h1>
                        <form onSubmit={this.handleSubmit} id="create-a-vehicle-form">
                            <div className="form-floating mb-3">
                                <input placeholder="name" onChange={this.handleModel} value={this.state.name} required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="picture" onChange={this.handlePicture} value={this.state.pictureUrl} required type="url" name="pictureUrl" id="pictureUrl" className="form-control" />
                                <label htmlFor="pictureUrl">Picture url (jpg,png)</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleManufacturer} required name="manufacturerId" value={this.state.manufacturerId} id="manufacturerId" className="form-select">
                                    <option value="">Manufacturer name</option>
                                    {this.state.manufacturers?.map(data => {
                                        return (
                                            <option key={data.id} value={data.id}>
                                                {data.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default VehicleForm
