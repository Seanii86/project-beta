import React from 'react';

function VehiclesList(props) {
    return (
        <div className="manufacturer-list">
            <h1 className="title" align="center">Vehicle Models</h1>
            <table className="table align-middle">
                <thead>
                    <tr>
                        <th>Model name</th>
                        <th>Model manufacturer</th>
                        <th>Picture of model</th>
                    </tr>
                </thead>
                <tbody>
                    {props.vehicles.map(vehicle => {
                        return (
                            <tr key={vehicle.id}>
                                <td>{vehicle.name}</td>
                                <td>{vehicle.manufacturer.name}</td>
                                <td><img className="border border-dark" src={vehicle.picture_url} height={150}/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default VehiclesList;
