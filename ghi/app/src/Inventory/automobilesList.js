import React from 'react';

function InventoryList(props) {
    return (
        <div className="autmobiles-list">
            <h1 className="title" align="center">List of automobiles</h1>
            <table className="table align-middle">
                <thead>
                    <tr>
                        <th>Model/Manufacturer</th>
                        <th>Vin #</th>
                        <th>Model year</th>
                        <th>Color</th>
                        <th>Recent photo</th>
                    </tr>
                </thead>
                <tbody>
                    {props.autos.map(auto => {
                        return (
                            <tr key={auto.id}>
                                <td>{auto.model.name}/{auto.model.manufacturer.name}</td>
                                <td>{auto.vin}</td>
                                <td>{auto.year}</td>
                                <td>{auto.color}</td>
                                <td><img src={auto.model.picture_url} height={150}/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default InventoryList;