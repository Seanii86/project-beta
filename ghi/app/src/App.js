import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';

import ManufacturerList from './Inventory/manufacturerList';
import ManufacturerForm from './Inventory/manufacturerForm';

import VehiclesList from './Inventory/vehicleModelsList';
import VehicleForm from './Inventory/vehicleModelForm';

import TechnicianForm from './services/newTechnicianForm';

import ServiceForm from './services/serviceAppointmentForm';
import ServiceAppsList from './services/serviceAppsList';
import ServiceHistory from './services/serviceHistory';

import InventoryList from './Inventory/automobilesList';
import AutomobileForm from './Inventory/automobileForm';

import Nav from './Nav';

import SalesPersonForm from './sales/SalesPersonForm';
import CustomerList from './sales/CustomerList';
import CustomerForm from './sales/CustomerForm';
import SalesPersonHistory from './sales/SalesPersonHistory';
import SalesRecordForm from './sales/SalesRecordForm';
import SalesRecordList from './sales/SalesRecordList';

function App(props) {
  if (props.manufacturers === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList manufacturers={props.manufacturers} />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>

          <Route path="technicians/new/" element={<TechnicianForm />} />

          <Route path="services">
            <Route path="" element={<ServiceAppsList  />} />
            <Route path="new" element={<ServiceForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>

          <Route path="vehicles">
            <Route path="" element={<VehiclesList vehicles={props.models} />} />
            <Route path="new" element={<VehicleForm />} />
          </Route>

          <Route path="inventory">
            <Route path="" element={<InventoryList autos={props.autos} />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>

          <Route path="customers" element={<CustomerList />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="salesperson" element={<SalesPersonForm />} />
          <Route path="salespersonform/new" element={<SalesPersonForm />} />
          <Route path="salespersonhistory" element={<SalesPersonHistory />} />
          <Route path="salesrecordlist" element={<SalesRecordList />} />
          <Route path="salesrecordlist/new" element={<SalesRecordForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
