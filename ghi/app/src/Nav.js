import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-danger">
      <div className="container-fluid">
        <NavLink className="navbar-brand text-dark" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">Manufacturers</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/">Manufacturers</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/manufacturers/new/">Create a manufacturer</NavLink>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">Vehicles</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/vehicles/">Vehicle Models</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/vehicles/new/">Create a Vehicle Model</NavLink>
              </ul>
            </li >

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">Automobiles</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/inventory/">Inventory</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/inventory/new/">Create a new inventory item</NavLink>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">Sales</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/salesrecordlist/">Sales History</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/salesrecordlist/new/">Create sales record</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/salesperson/">Create sales person</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/salespersonhistory/">sales person history</NavLink>
                <NavLink className="dropdown-item" aria-current="page" to="/customers/">Customer List</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/customers/new/">Add New Customer</NavLink>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                aria-expanded="false">Services</a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" aria-current="page" to="/services/">Service Appointments</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/services/new/">Create service appointment</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/services/history/">Finished records</NavLink>
                <NavLink className="dropdown-item text-dark" aria-current="page" to="/technicians/new/">Add a new Technician</NavLink>
              </ul>
            </li>
          </ul >
        </div >
      </div >
    </nav >
  )
}

export default Nav;
