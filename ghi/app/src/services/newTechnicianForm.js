import React from 'react';

class TechnicianForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            emp_number: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleName = this.handleName.bind(this)
        this.handleEmpNumber = this.handleEmpNumber.bind(this)
    }

    handleName(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleEmpNumber(event) {
        const value = event.target.value;
        this.setState({ emp_number: value })
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }

        const url = "http://localhost:8080/services/technicians/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json()
            const cleared = {
                name: '',
                emp_number: '',
            }
            this.setState(cleared)
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-a-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleName} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Technician's full name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmpNumber} value={this.state.emp_number} placeholder="name" required type="number" name="emp_number" id="emp_number" className="form-control" />
                                <label htmlFor="emp_number">Technician's employee number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default TechnicianForm