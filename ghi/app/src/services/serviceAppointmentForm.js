import React from 'react';

class ServiceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            customer_name: '',
            app_vin: '',
            date_time: '',
            reason: '',
            technician: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleName = this.handleName.bind(this)
        this.handleVin = this.handleVin.bind(this)
        this.handleDatetime = this.handleDatetime.bind(this)
        this.handleReason = this.handleReason.bind(this)
        this.handleTechnician = this.handleTechnician.bind(this)
    }



    handleName(event) {
        const value = event.target.value;
        this.setState({ customer_name: value })
    }
    handleVin(event) {
        const value = event.target.value;
        this.setState({ app_vin: value })
    }
    handleDatetime(event) {
        const value = event.target.value;
        this.setState({ date_time: value })
    }
    handleReason(event) {
        const value = event.target.value;
        this.setState({ reason: value })
    }
    handleTechnician(event) {
        const value = event.target.value;
        this.setState({ technician: value })
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.technicians

        const url = "http://localhost:8080/services/appointments/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json()
            const cleared = {
                customer_name: '',
                app_vin: '',
                date_time: '',
                reason: '',
                techs: '',
            }
            this.setState(cleared)
            window.location.reload()
        }
    }



    async componentDidMount() {
        const url = 'http://localhost:8080/services/technicians/'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json()
            this.setState({ technicians: data.technicians })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Service Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-a-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleName} value={this.state.customer_name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Customer full name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleVin} value={this.state.app_vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">Vehicle vin #</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleDatetime} value={this.state.date_time} placeholder="dateTime" required type="datetime-local" name="dateTime" id="dateTime" className="form-control" />
                                <label htmlFor="dateTime">Date & time of appointment</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" className="form-label">Reason for service</label>
                                <textarea onChange={this.handleReason} value={this.state.reason} className="form-control" rows="3" id="reason" name="reason"></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleTechnician} value={this.state.technician} required id="technician" name="technician" className="form-select">
                                    <option value="">Technician assigned</option>
                                    {this.state.technicians?.map(tech => {
                                        return (
                                            <option key={tech.id} value={tech.emp_number}>
                                                {tech.name}
                                            </option>
                                        )
                                    })};
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default ServiceForm