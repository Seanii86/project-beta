import React from 'react';

const removeAppointment = async (id) => {
    if (window.confirm('Are you sure want to cancel this appointment? 👻')) {
        const appointmentUrl = `http://localhost:8080/services/appointments/${id}`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            window.location.reload()
        } else {
            console.error(response)
        }
    }
}


const markFinished = async (id) => {
    if (window.confirm('Are you sure want to mark this appointment as finished?')) {
        const appointmentUrl = `http://localhost:8080/services/appointments/${id}/`
        const status = { "finished": true };
        const fetchConfig = {
            method: 'put',
            body: JSON.stringify(status),
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            window.location.reload()
        } else {
            console.error(response)
        }
    }
}




class ServiceAppsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointments: [],
            falseFlags: [],
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/services/appointments/'
        const response = await fetch(url)
        try {
            if (response.ok) {
                const data = await response.json()
                this.setState({ appointments: data.appointments })
                const falseFlags = []
                for (let apps of this.state.appointments) {
                    if (apps.finished === false) {
                        falseFlags.push(apps)
                    }

                }
                this.setState({ falseFlags: falseFlags })
            }
        } catch (error) {
            console.error(error)
        }
    }

    render() {
        return (
            <div className="manufacturer-list">
                <h1 className="title" align="center">Service Appointments</h1>
                <table className="table align-middle">
                    <thead>
                        <tr>
                            <th>V.I.P</th>
                            <th>VIN</th>
                            <th>Customer name</th>
                            <th>Date & time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.falseFlags.map(app => {
                            return (
                                <tr key={app.id}>
                                    <td>{app.vip}</td>
                                    <td>{app.app_vin}</td>
                                    <td>{app.customer_name}</td>
                                    <td>{app.date_time}</td>
                                    <td>{app.technician}</td>
                                    <td>{app.reason}</td>
                                    <td>
                                        <button className="btn btn-danger" onClick={() => removeAppointment(app.id)}>Cancel</button>
                                        <button className="btn btn-success" onClick={() => markFinished(app.id)}>Finish</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default ServiceAppsList;