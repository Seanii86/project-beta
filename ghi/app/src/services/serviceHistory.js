import React from 'react';
class ServiceAppsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointments: [],
            trueFlags: [],
            app_vin: '',
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/services/appointments/'
        const response = await fetch(url)
        try {
            if (response.ok) {
                const data = await response.json()
                this.setState({ appointments: data.appointments })
                const trueFlags = []
                for (let apps of this.state.appointments) {
                    if (apps.finished === true) {
                        trueFlags.push(apps)
                    }
                }
                this.setState({ trueFlags: trueFlags })
            }
        } catch (error) {
            console.error(error)
        }
    }


    handleVinChange = (event) => {
        const value = event.target.value
        this.setState({ app_vin: value })
    }

    handleSubmit = async (event) => {
        event.preventDefault()
        const data = this.state.appointments.filter((app) => {
            return app.app_vin === this.state.app_vin
        });
        this.setState({ filteredVin: data })
    }

    render() {
        return (
            <div className="manufacturer-list">
                <h1 className="title" align="center">Search records</h1>
                <div className="container">
                    <form className="d-flex" onSubmit={this.handleSubmit} id="service-history">
                        <input className="form-control me-2" onChange={this.handleVinChange} value={this.state.app_vin} type="text" placeholder="Search VIN#" requiredtype="text" name="vin" id="vin" />
                        <button className="btn btn-outline-success me-2" type="submit">Search</button>
                    </form>
                </div>
                <table className="table align-middle">
                    <thead>
                        <tr>
                            <th>V.I.P</th>
                            <th>VIN</th>
                            <th>Customer name</th>
                            <th>Date & time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.filteredVin? this.state.filteredVin.map(app => {
                            return (
                                <tr key={app.id}>
                                    <td>{app.vip}</td>
                                    <td>{app.app_vin}</td>
                                    <td>{app.customer_name}</td>
                                    <td>{app.date_time}</td>
                                    <td>{app.technician}</td>
                                    <td>{app.reason}</td>
                                </tr>
                            );
                        })
                    :null}
                    </tbody>
                </table>
            </div>
        )
    }
}
export default ServiceAppsList;